#!/usr/bin/env bash


source envs/.env-dev
cd glitchtip-backend
./manage.py $1
cd ..