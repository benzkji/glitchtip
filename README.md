# glitchtip

this repos deploys a glitchtip instance to a djangoeurope.com system user.


## Document / Questions / Feedback

- access-control-allow-origin: * sent? why? frontend issue reporting, maybe?
- corresponding versions? may synchronize back/frontend, to avoid confusion? (taiga does this)
- copy index.html
- copy dist, how exactly is it done
- pip install psycopg-binary needed
- npm install says 22 high severity vulnerabilities?

## Initialize

to init source repos of back and front:

```
git submodule init glitchtip-backend
git submodule init glitchtip-frontend
```

- make a virtualenv
- install poetry
- `pip install -r requirements.txt`
- `cd glitchtip-frontend ; npm install`
- `npm run build-prod`
- `mkdir ../glitchtip-backend/dist ; cp -r dist/glitchtip-frontend/* ../glitchtip-backend/dist/.`
- `cd ../glitchtip-backend`
- only when updating/initially: `poetry export --without-hashes --output ../requirements.txt`
- `pip install psycopg2-binary`
- `cp dist/index.html templates/.`


## Deploy

dailay fabric commands to remember:

```
fab live  # use live env
fab live deploy  # upgrade  glitchtip, deploy and restart
fab live upgrade_glitchtip  # upgrade backend (migrate, and all the stuff)
```
you need pip install fabric3 for now, to do this!



## Upgrade

on your computer, do a 

```
cd glitchtip-back
git checkout new-version-nb
poetry export --output ../requirements.txt
cd ..
cd glitchtip-front
git checkout new-version-nb-stable
npm run build prod
cp -r dist ../glitchtip-backend/.
cd ..
git commit -am'update to new-version-nb'
fab deploy upgrade_glitchtip
```

both glitchtip repos should be on the "stable" branch, or, better, checkout the same version number.


## Backup
