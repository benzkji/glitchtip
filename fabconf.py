import os

from fabric.api import task, env
from fabric.api import run, roles, cd, execute, hide, puts
from fabric.operations import put


env.is_python3 = True
env.project_name = 'glitchtip'
env.repository = 'ssh://bnzk@gitea.bnzk.ch:61005/bnzk_internal/{project_name}.git'.format(**env)
# env.repository = 'git@bitbucket.org:bnzk/{project_name}.git'.format(**env)
env.sites = ('glitchtip', )
env.is_postgresql = True  # False for mysql! only used for put/get_db
env.needs_main_nginx_files = True
env.is_supervisord = True
env.is_nginx_gunicorn = True
env.is_uwsgi = False
env.is_apache = False
env.remote_ref = 'origin/main'
# these will be checked for changes
env.requirements_files = [
    'requirements.txt',
]
# this is used with pip install -r
env.requirements_file = env.requirements_files[0]


# ==============================================================================
# Tasks which set up deployment environments
# ==============================================================================


@task
@roles('web', 'db')
def custom_bootstrap():
    from fabfile import dj
    # supervisor baby!? run('mkdir -p ~/.config/systemd/user'.format(**env))
    with cd(env.project_dir):
        run('git submodule init glitchtip-backend'.format(**env))
        run('git submodule update'.format(**env))
    # with cd(os.path.join(env.project_dir, 'glitchtip-back', 'settings')):
    #     run('ln -s ../../settings/local_{env_prefix}.py local.py'.format(**env))
    # with cd(os.path.join(env.project_dir, 'glitchtip-front-dist', 'dist')):
    #     run('ln -s ../../settings/conf_{env_prefix}.json conf.json'.format(**env))
    # dj('migrate --noinput')
    # dj('loaddata initial_user')
    # dj('loaddata initial_project_templates')


@task
@roles('web', 'db')
def upgrade_glitchtip():
    from fabfile import dj
    with cd(env.project_dir):
        run('git submodule update'.format(**env))
        remote_path = os.path.join(
            env.project_dir,
            'glitchtip-backend',
            'templates',
        )
        put(local_path='glitchtip-backend/templates/index.html', remote_path=remote_path)
        remote_path = os.path.join(
            env.project_dir,
            'glitchtip-backend',
            'dist',
        )
        run('mkdir --parent {}'.format(remote_path))
        put(local_path='glitchtip-backend/dist', remote_path=remote_path)
    dj('migrate --noinput')


@task
def live():
    """
    Use the live deployment environment.
    """
    env.env_prefix = 'live'
    env.deploy_crontab = True
    env.env_file = 'envs/.env-live'
    env.main_user = 'glitchtip'.format(**env)
    server = '{main_user}@s29.wservices.ch'.format(**env)
    env.roledefs = {
        'web': [server],
        'db': [server],
    }
    generic_env_settings()


@task
def stage():
    """
    Use the sandbox deployment environment on xy.bnzk.ch.
    """
    # exit("no stage for glitchtip!")
    env.env_prefix = 'stage'
    env.deploy_crontab = False
    env.main_user = '{project_name}'.format(**env)
    server = '{main_user}@s29.wservices.ch'.format(**env)
    env.roledefs = {
        'web': [server],
        'db': [server],
    }
    generic_env_settings()


def generic_env_settings():
    if not getattr(env, 'deploy_crontab', None):
        env.deploy_crontab = False
    env.project_dir = '/home/{main_user}/sites/{project_name}-{env_prefix}'.format(**env)
    env.project_conf = 'glitchtip.settings'.format(**env)
    env.custom_manage_py_root = '{project_dir}/glitchtip-backend'.format(**env)
    env.virtualenv_dir = '{project_dir}/virtualenv'.format(**env)
    env.gunicorn_restart_command = 'systemctl --user daemon-reload && systemctl --user restart {site}-{env_prefix}.service'
    env.gunicorn_stop_command = '~/init/{site}-{env_prefix}.sh stop'
    env.nginx_restart_command = '~/init/nginx.sh restart'
    # not needed with uwsgi emporer mode, cp is enough
    # env.uwsgi_restart_command = 'touch $HOME/uwsgi.d/{site}-{env_prefix}.ini'


stage()
